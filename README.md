[![godoc](https://godoc.org/golang.spin-2.net/astpkg?status.svg)](https://godoc.org/golang.spin-2.net/astpkg)


### goastpkg

Package goastpkg uses [`golang.org/x/tools/go/packages`][gopkgs] to load full ASTs for a
package and its dependencies, then generates a [`*go/ast.Package`][astpkg] for the
requested package (and its dependencies).

This is particularly useful when one wants all Obj references resolved in
indentifiers, both within a package (between files) and across packages.

This package is new and somewhat experimental, and currently only resolves one
layer of dependencies due to failures encountered when trying to resolve deep
dependencies.


### usage

This package is designed to have no knobs, so one can get the full AST of a
package with one call.

```go
package main

import (
    "context"

    goastpkg "golang.spin-2.net/astpkg"
)

func main() {
    pkg, err := goastpkg.Load(context.Background, "net")
    // Do something interesting with pkg.PkgAST
}
```

Note: the [`token.FileSet`][fset] used for parsing is exposed as the `Fset`
member of all `packages.Package` structs.

[gopkgs]: https://godoc.org/golang.org/x/tools/go/packages
[astpkg]: https://golang.org/pkg/go/ast#Package
[fset]: https://golang.org/pkg/go/token#FileSet
