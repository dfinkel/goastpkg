// BSD 3-Clause License
//
// Copyright (c) 2019, David Finkel
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// * Redistributions of source code must retain the above copyright notice, this
//   list of conditions and the following disclaimer.
//
// * Redistributions in binary form must reproduce the above copyright notice,
//   this list of conditions and the following disclaimer in the documentation
//   and/or other materials provided with the distribution.
//
// * Neither the name of the copyright holder nor the names of its
//   contributors may be used to endorse or promote products derived from
//   this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

// Package goastpkg uses golang.org/x/tools/go/packages to load full ASTs for a
// package and its dependencies, then generates a *go/ast.Package for the
// requested package (and its dependencies).
//
// This is particularly useful when one wants all Obj references resolved in
// indentifiers, both within a package (between files) and across packages.
package goastpkg // import "golang.spin-2.net/astpkg"

import (
	"context"
	"fmt"
	"go/ast"
	"go/build"
	"go/token"
	"path/filepath"

	"golang.org/x/tools/go/packages"
)

// Package contains the ast.Package and packages.Package pointers for a package
// and all its (transitive) dependencies.
type Package struct {
	Path   string
	PkgAST *ast.Package
	Pkg    *packages.Package
	// CommentMap per file in the package
	PkgCommentMaps map[*ast.File]ast.CommentMap

	// Deps and DepsAST are keyed by import path.
	DepsAST map[string]*ast.Package
	Deps    map[string]*packages.Package
}

func findPkg(pkgs []*packages.Package, pkgName string) *packages.Package {
	for _, pkg := range pkgs {
		if pkg.PkgPath == pkgName {
			return pkg
		}
	}
	return nil
}

// Load reads in the requested package, populating a *Package with the
// AST/types information.
func Load(ctx context.Context, pkgName string) (*Package, error) {
	cfg := packages.Config{
		Mode:    packages.LoadAllSyntax,
		Context: ctx,
		Tests:   false,
	}
	// Include the "builtin" package as the universe scope
	pkgs, err := packages.Load(&cfg, pkgName, "builtin")
	if err != nil {
		return nil, fmt.Errorf("failed to load/parse packages: %s", err)
	}
	pkg := findPkg(pkgs, pkgName)
	ret := Package{
		Path:           pkgName,
		Pkg:            pkg,
		PkgCommentMaps: genCommentMap(pkg),
		Deps:           collatePkgs(pkgs),
	}
	universe, err := genUniverse(findPkg(pkgs, "builtin"))
	if err != nil {
		return nil, fmt.Errorf("failed to acquire builtin types for universe scope: %s",
			err)
	}
	importer := pkgImporter{
		PkgsAST:  map[string]*ast.Package{},
		Pkgs:     ret.Deps,
		Universe: universe,
	}
	pkga, err := importer.mkpkg(pkg)
	if err != nil {
		return nil, fmt.Errorf("error resolving symbols into ast.Package: %s", err)
	}
	ret.PkgAST = pkga
	ret.DepsAST = importer.PkgsAST

	return &ret, nil
}

func genCommentMap(pkg *packages.Package) map[*ast.File]ast.CommentMap {
	out := make(map[*ast.File]ast.CommentMap, len(pkg.Syntax))
	for _, f := range pkg.Syntax {
		out[f] = ast.NewCommentMap(pkg.Fset, f, f.Comments)
	}
	return out
}

func genUniverse(builtinPkg *packages.Package) (*ast.Scope, error) {
	files := collateASTFiles(builtinPkg.Fset, builtinPkg.Syntax)
	pkg, err := ast.NewPackage(builtinPkg.Fset, files, nil, nil)
	if err != nil {
		return nil, err
	}
	return pkg.Scope, nil
}

func collatePkgs(pkgs []*packages.Package) map[string]*packages.Package {
	out := make(map[string]*packages.Package, len(pkgs))

	addDep := func(pkg *packages.Package) bool {
		out[pkg.PkgPath] = pkg
		return true
	}

	packages.Visit(pkgs, addDep, nil)
	return out
}

type pkgImporter struct {
	PkgsAST  map[string]*ast.Package
	Pkgs     map[string]*packages.Package
	Universe *ast.Scope
}

func newObj(pkg *ast.Package) *ast.Object {
	obj := ast.NewObj(ast.Pkg, pkg.Name)
	obj.Decl = pkg
	obj.Data = pkg.Scope
	return obj
}

func collateASTFiles(fs *token.FileSet, files []*ast.File) map[string]*ast.File {
	out := make(map[string]*ast.File, len(files))
	for _, file := range files {
		out[fs.File(file.Pos()).Name()] = file
	}
	return out
}

func (p *pkgImporter) importer(imports map[string]*ast.Object, path string) (*ast.Object, error) {
	if obj, hasObj := imports[path]; hasObj {
		return obj, nil
	}

	if pkga, ok := p.PkgsAST[path]; ok {
		obj := newObj(pkga)
		imports[path] = obj
		return obj, nil
	}

	// We don't have a cached package AST

	pkg, ok := p.Pkgs[path]
	if !ok {
		return nil, fmt.Errorf("unknown package %q", path)
	}

	pkga, err := p.mkpkg(pkg)
	if err != nil {
		return nil, err
	}
	p.PkgsAST[path] = pkga
	obj := newObj(pkga)
	imports[path] = obj
	return obj, nil

}

func (p *pkgImporter) mkpkg(pkg *packages.Package) (*ast.Package, error) {
	files := collateASTFiles(pkg.Fset, pkg.Syntax)
	// Unfortunately, some core packages (e.g. math/unsafe) defy resolution, so we
	// have to resort to using a stub importer after the first level of imports.
	return ast.NewPackage(pkg.Fset, files, p.stubber, p.Universe)
}

func (p *pkgImporter) stubber(imports map[string]*ast.Object, path string) (
	pkg *ast.Object, err error) {

	if build.IsLocalImport(path) {
		return nil, fmt.Errorf("relative import outside local scope: %s", path)
	}

	if obj, ok := imports[path]; ok {
		return obj, nil
	}
	if pkga, ok := p.PkgsAST[path]; ok {
		obj := newObj(pkga)
		imports[path] = obj
		return obj, nil
	}
	obj := ast.NewObj(ast.Pkg, filepath.Base(path))
	imports[path] = obj
	return obj, nil
}
