package goastpkg

import (
	"context"
	"go/ast"
	"testing"
)

func TestParsePkgs(t *testing.T) {
	ctx := context.Background()
	for pkgNameIn, paramsIn := range map[string]struct {
		expectedFunctions []string
	}{
		"context": {
			expectedFunctions: []string{"WithCancel", "WithDeadline",
				"WithTimeout", "Background", "TODO", "WithValue"},
		},
		"golang.org/x/tools/go/packages": {
			expectedFunctions: []string{"Visit", "PrintErrors", "Load"},
		},
		"golang.spin-2.net/astpkg": {
			expectedFunctions: []string{"Load", "genUniverse", "newObj"},
		},
	} {
		pkgName := pkgNameIn
		params := paramsIn
		t.Run(pkgName, func(t *testing.T) {
			t.Parallel()
			pkg, err := Load(ctx, pkgName)
			if err != nil {
				t.Fatalf("unexpected error parsing package %q: %s",
					pkgName, err)
			}
			if pkg == nil {
				t.Fatalf("unexpectedly nil package %q", pkgName)
			}
			if pkgName != pkg.Path {
				t.Errorf("unexpected package path %q populated, expected %q",
					pkg.Path, pkgName)
			}
			if len(pkg.Pkg.Errors) > 0 {
				t.Errorf("errors found in primary package: %v", pkg.Pkg.Errors)
			}
			for _, fn := range params.expectedFunctions {
				lookupObj := pkg.PkgAST.Scope.Lookup(fn)
				if lookupObj == nil {
					t.Errorf("unexpectedly nil lookup for %q", fn)
					continue
				}
				if lookupObj.Kind != ast.Fun {
					t.Errorf("object kind for %s is %s not Fun", fn, lookupObj.Type)
				}
				funcDecl, isFunc := lookupObj.Decl.(*ast.FuncDecl)
				if !isFunc {
					t.Errorf("unexpected Decl type on function: %T", lookupObj.Decl)
					continue
				}
				if funcDecl.Body == nil {
					t.Errorf("function body is nil for %s", fn)
					continue
				}
			}
		})
	}
}
