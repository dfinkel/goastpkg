package importmgr

import (
	"path/filepath"
	"strings"
	"unicode"
)

type collisionTracker struct {
	// Map from import name to package path
	importNames map[string]string
}

func (c *collisionTracker) findName(origPkgName, pkgPath string) string {
	pkgName := c.eliminateCollision(origPkgName, pkgPath)
	c.importNames[pkgName] = pkgPath
	return pkgName
}

func (c *collisionTracker) eliminateCollision(origPkgName, pkgPath string) string {
	// first, check whether the name is taken in the first place.
	if _, taken := c.importNames[origPkgName]; !taken {
		return origPkgName
	}
	pkgName := origPkgName
	mangledPath := mangleImportPathForPrefix(pkgPath)
	for i := range mangledPath {
		tmpName := mangledPath[:i+1] + origPkgName
		if _, taken := c.importNames[tmpName]; !taken {
			return tmpName
		}
	}

	pkgName = mangledPath + pkgName

	// As a fallback, we'll just add 'z's to the end of the import name until it
	// doesn't collide. (we should realistically never get here but we might as
	// well make the imports look interesting if we do)
	for {
		if _, taken := c.importNames[pkgName]; taken {
			// there's a naming collision. Append "z" to the end of the package
			// name until it becomes unique.
			pkgName += "z"
			continue
		}
		return pkgName
	}
}

func isBlacklistedDomain(d string) bool {
	switch d {
	case "github.com", "gitlab.com", "bitbucket.com", "gopkg.in":
		return true
	default:
		return false
	}
}

func strMapLetterDigitOnlyChars(r rune) rune {
	if unicode.IsDigit(r) || unicode.IsLetter(r) {
		return r
	}
	return -1
}

func cleanIdent(s string) string {
	if len(s) == 0 {
		return ""
	}
	retPrefix := ""
	q := -1
	for i, r := range s {
		if unicode.IsLetter(r) {
			retPrefix = string(r)
			q = i
			break
		}
	}
	if q == -1 {
		return ""
	}
	q++
	if q == len(s) {
		return retPrefix
	}
	return retPrefix + strings.Map(strMapLetterDigitOnlyChars, s[q:])
}

func mangleImportPathForPrefix(pkgPath string) string {
	// reorder ident-characters from the non-terminal component of the import path
	// (ignoring Domain name-like prefixes in some cases)
	//  - go.opencensus.io/... needs to derive characters from "opencensusio"
	//  - golang.org/x/tools/... should probably derive from "golangxtools"
	//  - gitlab.com/dfinkel/goastpkg should derive from "dfinkel"
	//  - gitlab.com/dfinkel/goastpkg/importmgr should derive from "dfinkelastpkg"
	//  - text/template has to derive from "text"
	//  - html/template has to derive from "html"
	//  - github.com/dfinkel/foobar/vendor/github.com/foo/bar should derive
	//    from "vfoobardfinkel"
	//  - github.com, gitlab.com, bitbucket.com, gopkg.in, etc should never be
	//    used disambiguation.

	// remove the last component (since that's already used for the base most
	// of the time)
	dir := filepath.Dir(pkgPath)
	cmps := strings.Split(dir, "/")
	if len(cmps) == 0 {
		return ""
	}
	// track whether there's a vendor directory component, since we want to
	// derive characters from that.
	hasVendor := false

	b := strings.Builder{}
CMPS:
	for i, c := range cmps {
		if i == 0 && isBlacklistedDomain(c) {
			continue
		}
		switch c {
		case "vendor":
			hasVendor = true
		case "golang.org":
			b.WriteString("golang")
			continue CMPS
		case "":
			// empty string, just continue
			continue CMPS
		}
		cleanc := cleanIdent(c)
		cleanc = strings.TrimPrefix(cleanc, "go")
		b.WriteString(cleanc)
	}

	out := b.String()

	if hasVendor {
		out = "v" + out
	}

	return out
}
