package importmgr

import "testing"

func TestCollisionMgr(t *testing.T) {
	c := collisionTracker{
		importNames: map[string]string{},
	}
	for _, i := range []struct {
		path, inName, expectedName string
	}{{
		path:         "html/template",
		inName:       "template",
		expectedName: "template",
	}, {
		path:         "text/template",
		inName:       "template",
		expectedName: "ttemplate",
	}, {
		path:         "github.com/spf13/viper",
		inName:       "viper",
		expectedName: "viper",
	}, {
		path:         "github.com/dfinkel/viper",
		inName:       "viper",
		expectedName: "dviper",
	}, {
		path:         "gitlab.com/dfinkel/viper",
		inName:       "viper",
		expectedName: "dfviper",
	}, {
		path:         "bitbucket.com/dfinkel/viper",
		inName:       "viper",
		expectedName: "dfiviper",
	}, {
		path:         "github.com/dfi/viper",
		inName:       "viper",
		expectedName: "dfiviperz",
	}, {
		path:         "github.com/dfinkel/view",
		inName:       "view",
		expectedName: "view",
	}, {
		path:         "go.opencensus.io/view",
		inName:       "view",
		expectedName: "oview",
	}, {
		path:         "gitlab.com/dfinkel/someproject/vendor/go.opencensus.io/view",
		inName:       "view",
		expectedName: "vview",
	}, {
		path:         "golang.org/x/tools/packages/view",
		inName:       "view",
		expectedName: "gview",
	}, {
		path:         "gitlab.com/68667/3445/123",
		inName:       "view",
		expectedName: "viewz",
	},
	} {
		out := c.findName(i.inName, i.path)
		if out != i.expectedName {
			t.Errorf("unexpected output %q; %+q", out, i)
		}
	}
}

func TestManglePath(t *testing.T) {
	t.Parallel()
	for in, expected := range map[string]string{
		"go.opencensus.io/view":            "opencensusio",
		"golang.org/x/tools/packages/view": "golangxtoolspackages",
		"github.com/dfinkel/view":          "dfinkel",
	} {
		out := mangleImportPathForPrefix(in)
		if out != expected {
			t.Errorf("mangled %q = %q; expected %q", in, out, expected)
		}
	}
}
