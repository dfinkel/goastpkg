// BSD 3-Clause License
//
// Copyright (c) 2019, David Finkel
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// * Redistributions of source code must retain the above copyright notice, this
//   list of conditions and the following disclaimer.
//
// * Redistributions in binary form must reproduce the above copyright notice,
//   this list of conditions and the following disclaimer in the documentation
//   and/or other materials provided with the distribution.
//
// * Neither the name of the copyright holder nor the names of its
//   contributors may be used to endorse or promote products derived from
//   this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

// Package importmgr provides the ImportMgr type for tracking a go file's
// imports when generating code by manipulating the Go AST.
package importmgr // import "golang.spin-2.net/astpkg/importmgr"

import (
	"context"
	"fmt"
	"go/ast"
	"go/token"
	"path/filepath"
	"strconv"

	"golang.org/x/tools/go/packages"

	goastpkg "golang.spin-2.net/astpkg"
)

// ImportMgr maintains state on the imports for a file.
type ImportMgr struct {
	deps       *goastpkg.Package
	imports    map[string]*ast.ImportSpec
	importObjs map[string]*ast.Object

	c collisionTracker
}

// NewImportMgr constructs an empty ImportMgr for the package-set provided.
func NewImportMgr(deps *goastpkg.Package) *ImportMgr {
	return &ImportMgr{
		deps:       deps,
		imports:    map[string]*ast.ImportSpec{},
		importObjs: map[string]*ast.Object{},
		c: collisionTracker{
			importNames: map[string]string{},
		},
	}
}

func (i *ImportMgr) impAndObj(pkgName, pkgPath string) (*ast.ImportSpec, *ast.Object) {
	pkgName = i.c.findName(pkgName, pkgPath)

	obj := ast.NewObj(ast.Pkg, pkgName)

	importspec := &ast.ImportSpec{
		Name: nil,
		Path: &ast.BasicLit{
			Kind:  token.STRING,
			Value: strconv.Quote(pkgPath),
		},
	}
	obj.Decl = importspec
	// Make it obvious what the import name is if it's not present.
	if pkgName != filepath.Base(pkgPath) {
		importspec.Name = ast.NewIdent(pkgName)
	}
	return importspec, obj
}

// GetPkgIdent adds an appropriate import for the import if necessary and
// returns an *ast.Ident referencing that package. The return value should only
// be used once within an AST, as the source location fields within an Ident
// (or any ast node) must be unique.
func (i *ImportMgr) GetPkgIdent(path string) *ast.Ident {
	if obj, ok := i.importObjs[path]; ok {
		return identFromObj(obj)
	}
	// time to find some data on this package.
	// Some key questions to ask:
	//  - What does it call itself?
	//  - Where is it from?
	//  - What color are its eyes?
	//  - Does it enjoy long walks on the beach?

	// first check whether it's present in our deps map.
	if pkg, ok := i.deps.Deps[path]; ok {
		// it's here, so our job is now easy.
		importspec, obj := i.impAndObj(pkg.Name, path)
		i.importObjs[path] = obj
		i.imports[path] = importspec
		return identFromObj(obj)
	}
	return nil
}

// GetPkgIdentWithLoadFallback calls GetPkgIdent, and if it returns nil, uses
// golang.org/x/tools/packages.Load to lookup the package name.
func (i *ImportMgr) GetPkgIdentWithLoadFallback(
	ctx context.Context, path string) (*ast.Ident, error) {
	id := i.GetPkgIdent(path)
	if id != nil {
		return id, nil
	}
	// Load the package information
	cfg := packages.Config{
		Mode:    packages.LoadFiles,
		Context: ctx,
		Tests:   false,
	}
	pkgs, err := packages.Load(&cfg, path)
	if err != nil {
		return nil, fmt.Errorf("failed to parse package %q: %s", path, err)
	}
	if len(pkgs) == 0 {
		return nil, fmt.Errorf("failed to get package %q (zero packages matched path)", path)
	}

	for _, pkg := range pkgs {
		if pkg.PkgPath == path {
			importspec, obj := i.impAndObj(pkg.Name, path)
			i.importObjs[path] = obj
			i.imports[path] = importspec
			return identFromObj(obj), nil
		}
	}
	return nil, fmt.Errorf("package %q not found", path)
}

// ImportFile imports the existing imports/import-objects from an existing
// ast.File.
func (i *ImportMgr) ImportFile(file *ast.File) error {
	for idx, imp := range file.Imports {
		path, err := strconv.Unquote(imp.Path.Value)
		if err != nil {
			return fmt.Errorf("failed to unquote import %d: %s",
				idx, imp.Path.Value)
		}
		name := filepath.Base(path)
		// Check whether it's present in our deps map.
		if pkg, ok := i.deps.Deps[path]; ok {
			name = pkg.Name
		}
		if imp.Name != nil {
			name = imp.Name.Name
		}
		obj := file.Scope.Lookup(name)
		if obj == nil {
			obj = ast.NewObj(ast.Pkg, name)
		}
		i.importObjs[path] = obj
		i.imports[path] = imp
	}
	return nil
}

// Specs returns a slice of *ast.ImportSpecs appropriate for insertion into an
// ast.File.Imports.
func (i *ImportMgr) Specs() []*ast.ImportSpec {
	specs := make([]*ast.ImportSpec, 0, len(i.imports))
	for _, imp := range i.imports {
		specs = append(specs, imp)
	}
	return specs
}

func identFromObj(obj *ast.Object) *ast.Ident {
	id := ast.NewIdent(obj.Name)
	id.Obj = obj

	return id
}
