package importmgr

import (
	"context"
	"go/ast"
	"path/filepath"
	"strconv"
	"testing"

	goastpkg "golang.spin-2.net/astpkg"
)

func TestImportFile(t *testing.T) {
	t.Parallel()
	ctx := context.Background()
	pkgName := "golang.spin-2.net/astpkg/importmgr"
	pkg, err := goastpkg.Load(ctx, pkgName)
	if err != nil {
		t.Fatalf("unexpected error parsing package %q: %s",
			pkgName, err)
	}
	if len(pkg.Pkg.Syntax) == 0 {
		t.Fatalf("no files parsed for package %q", pkgName)
	}
	var f *ast.File
	for fname, fs := range pkg.PkgAST.Files {
		if filepath.Base(fname) == "mgr.go" {
			f = fs
		}
	}
	if f == nil {
		t.Fatalf("failed to lookup pkg file: %+v present", pkg.PkgAST.Files)
	}

	imp := NewImportMgr(pkg)
	if err := imp.ImportFile(f); err != nil {
		t.Errorf("failed to import file contents: %s", err)
	}

	specs := imp.Specs()
	if specs == nil {
		t.Errorf("nil specs")
	}
	expectedImports := map[string]string{
		"context":                        "context",
		"fmt":                            "fmt",
		"go/ast":                         "ast",
		"go/token":                       "token",
		"path/filepath":                  "filepath",
		"strconv":                        "strconv",
		"golang.spin-2.net/astpkg":       "goastpkg",
		"golang.org/x/tools/go/packages": "packages",
	}
	for _, spec := range specs {
		specPath, err := strconv.Unquote(spec.Path.Value)
		if err != nil {
			t.Errorf("failed to unquote path (%s): %s", spec.Path.Value, err)
			continue
		}
		if expectedName, ok := expectedImports[specPath]; ok {
			id := imp.GetPkgIdent(specPath)
			if id.Name != expectedName {
				t.Errorf("unexpected package ident name: %q expected, %q seen",
					expectedName, id.Name)
			}
			delete(expectedImports, specPath)
		} else {
			t.Errorf("duplicate or unexpected import: %q", specPath)
		}
	}
	if len(expectedImports) > 0 {
		t.Errorf("expected imports unfound: %+v", expectedImports)
	}
}

func TestAddImports(t *testing.T) {
	t.Parallel()
	ctx := context.Background()
	pkgName := "golang.spin-2.net/astpkg/importmgr"
	pkg, err := goastpkg.Load(ctx, pkgName)
	if err != nil {
		t.Fatalf("failed to load/parse packages: %s", err)
	}
	imp := NewImportMgr(pkg)

	for _, i := range []struct {
		path, expectedName string
	}{{
		path:         "html/template",
		expectedName: "template",
	}, {
		path:         "text/template",
		expectedName: "ttemplate",
	}, {
		path:         "golang.spin-2.net/astpkg/importmgr",
		expectedName: "importmgr",
	}, {
		path:         "golang.spin-2.net/astpkg",
		expectedName: "goastpkg",
	}, {
		path:         "context",
		expectedName: "context",
	}} {
		id, err := imp.GetPkgIdentWithLoadFallback(ctx, i.path)
		if err != nil {
			t.Errorf("error loading path %q: %s", i.path, err)
			continue
		}
		if id.Name != i.expectedName {
			t.Errorf("unexpected pkg import name: %q; expected: %q",
				id.Name, i.expectedName)
		}
	}
}
